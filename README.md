# Project Title- Mall heat map plotter on dashboard

Project description- Detect and count people and create a time series graph which shows 
which times of the day there are maximum people spotted at the entrance of the mall

![project-image](https://static.wingify.com/gcp/uploads/sites/3/2020/02/Feature-image_Heatmap-Dashboard.png)

## Shunya Stack 
Project is built by using the Shunya stack.

-   ShunyaOS is a lightweight Operating system with built-in support for AI and IOT.
-   Shunya Stack Low Code platform to build AI, IoT and AIoT products, that allows you to rapidly create and deploy AIoT products with ease.

For more information on ShunyaOS see : http://demo.shunyaos.org


## Documentation 
For developers see detailed Documentation on the components of the project in the [Wiki](https://gitlab.com/matrix_vskm/mall-heat-map-plotter-on-dashboard/-/issues/3)

## Project Overview 

1.  [Project Plan Excel](https://docs.google.com/spreadsheets/d/1G5_z7RdnCgFZvkxCRqYc6tbYx6AWXPb7ZbhtgZg9dFU/edit#gid=0)
2.  [Blog]( https://mallheatmapplotter.blogspot.com/2021/04/detect-crowd-presence-with-mall-heat.html)
3.  [Video](https://www.canva.com/design/DAEacUHW7Qo/1w3QltAcRWW-WdZPQmMXHA/watch?utm_content=DAEacUHW7Qo&utm_campaign=designshare&utm_medium=link&utm_source=publishsharelink)


## Contributing
Help us improve the project.

Ways you can help:

1.  Choose from the existing issue and work on those issues.
2.  Feel like the project could use a new feature, make an issue to discuss how it can be implemented and work on it.
3.  Find a bug create an Issue and report it.
4.  Review Issues or Merge Requests, give the developers the feedback.
5.  Fix Documentation.

## Contributors 

#### Team Lead
1. Saurabh Jejurkar (@saurabh_v_j)

#### Active Contributors.

1.  Vedant Jore - @vedant01
2.  Kritika Kshirsagar - @kshirsagarkritika
3.  Mansi Muluk - @manasimuluk272 

