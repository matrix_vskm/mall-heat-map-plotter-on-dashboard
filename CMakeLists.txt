# Set the CMake minimum version
cmake_minimum_required(VERSION 3.5)

# Declare the Project version and the Language that the project is written in
project(aiotMicro VERSION 0.0.1)

# Find and add the OpenCV dependencies
find_package( OpenCV REQUIRED )

find_package(PkgConfig REQUIRED) 

# these calls create special `PkgConfig::<MODULE>` variables
pkg_check_modules(SHUNYA_INTERFACES REQUIRED IMPORTED_TARGET shunyaInterfaces)

# This is equal to QMAKE_CXX_FLAGS += -std=c++0x
set(CMAKE_CXX_STANDARD 11)
# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)

# Global Install Directories
include(GNUInstallDirs)

set(ALL_SOURCE_FILES 
    ${CMAKE_CURRENT_SOURCE_DIR}/src/main.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/exutils.cpp)

add_executable(${PROJECT_NAME} ${ALL_SOURCE_FILES})

target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include)
target_include_directories(${PROJECT_NAME} PUBLIC ${OpenCV_INCLUDE_DIRS})
target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/src)
target_link_libraries(${PROJECT_NAME} PUBLIC ${OpenCV_LIBS} siVideo PkgConfig::SHUNYA_INTERFACES)
#target_link_libraries(${PROJECT_NAME} PUBLIC siCore siYaml siPeripheral siConn siCloud siIiot siDash siAlerts siFunctions)
target_link_libraries(${PROJECT_NAME} PUBLIC -lpthread)

add_custom_command(OUTPUT codecheck.txt
    COMMAND /usr/bin/cppcheck --dump --quiet ${ALL_SOURCE_FILES}
    COMMAND python3 
        /usr/share/cppcheck/addons/misra.py 
        --rule-texts=/usr/share/cppcheck/MISRA_C_2012.txt 
        ${DUMP_FILES} || exit 0
    COMMAND rm -rf ${DUMP_FILES}
    WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
    COMMENT "Checking for MISRA Code compliance"
    VERBATIM
)

add_custom_target(codestyle DEPENDS codecheck.txt)
